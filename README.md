# WMF SparkSQLCLIDriver

Fixes an issue in which `org.apache.spark.sql.hive.thriftserver.SparkSQLCLIDriver` can not load HTTPS URLs that contain url-encoded bits.

See https://phabricator.wikimedia.org/T333001 for more info.