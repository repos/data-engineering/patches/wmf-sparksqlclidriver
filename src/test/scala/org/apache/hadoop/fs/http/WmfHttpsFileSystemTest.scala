package org.apache.hadoop.fs.http

import org.apache.hadoop.fs.Path
import org.scalatest.funsuite.AnyFunSuite

import java.net.URL

class WmfHttpsFileSystemTest extends AnyFunSuite {

  test("Test that a Path converted to a URL honors any url-encoded parts") {
    val path : Path = new Path("https://gitlab.wikimedia.org/api/v4/projects/1261/repository/files/test%2Ftest_hql.hql/raw?ref=0e4d2a9")
    val url : URL = new URL(path.toString)

    assert(path.toString == url.toString)
  }

  test("Test that a Path with a https schema indeed returns that when asked") {
    val path : Path = new Path("https://gitlab.wikimedia.org/api/v4/projects/1261/repository/files/test%2Ftest_hql.hql/raw?ref=0e4d2a9")

    assert(path.toUri.getScheme == "https")
  }

  test("Test that WmfHttpsFileSystem can open a URL with url-encoded parts") {
    val path : Path = new Path("https://gitlab.wikimedia.org/api/v4/projects/1261/repository/files/test%2Ftest_hql.hql/raw?ref=0e4d2a9")
    val fs: WmfHttpsFileSystem = new WmfHttpsFileSystem()

    // if we do not throw, then we were able to open.
    val input = fs.open(path, 0)
    import org.apache.commons.io.IOUtils
    import java.nio.charset.StandardCharsets
    val result = IOUtils.toString(input, StandardCharsets.UTF_8)
    input.close()

    assert(result.contains("SELECT COUNT(1)"))
  }
}
