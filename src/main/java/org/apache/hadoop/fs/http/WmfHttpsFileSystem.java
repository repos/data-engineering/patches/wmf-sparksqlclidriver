/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.hadoop.fs.http;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
/**
 * A Filesystem that reads from HTTPS endpoint, without mangling the URL by calling toUri() on it.
 */
public class WmfHttpsFileSystem extends CopiedAbstractHttpFileSystem {
  @Override
  public String getScheme() {
    return "https";
  }

  @Override
  public FSDataInputStream open(Path path, int bufferSize) throws IOException {
    // Original code at org.apache.hadoop.fs.http.AbstractHttpFileSystem.open
    // uses path.toUri() which, for some reason, tries to url-encode our already url-encoded URL.
    // Here we avoid that by going directly from Path to URL object.
    // See https://phabricator.wikimedia.org/T333001.
    URL url = new URL(path.toString());
    InputStream in = url.openStream();
    return new FSDataInputStream(new HttpDataInputStream(in));
  }
}
